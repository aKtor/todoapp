package it.jdk.lab.todo.presistence;

/**
 * Created by Jad1 on 28/10/13.
 */

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;

import it.jdk.lab.todo.Contract;

public class TodoContentProvider extends ContentProvider {

    private TodoOpenHelper todoOpenHelper;

    @Override
    public boolean onCreate() {

        todoOpenHelper = new TodoOpenHelper(getContext(),
                Constants.DATABASE_NAME, null,
                Constants.DATABASE_VERSION);

        return true;
    }

    /**
     * Return a string that identifies the MIME type
     for a Content Provider URI
     */
    @Override
    public String getType(Uri uri) {

        switch (Constants.URI_MATCHER.match(uri)) {
            case Constants.SINGLE_ROW: return "vnd.android.cursor.item/vnd.todo"; // for single records
            case Constants.ALLROWS: return "vnd.android.cursor.dir/vnd.todo"; // for zero or multiple record
            default: throw new IllegalArgumentException("Unsupported URI: " + uri);
        }
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArg, String sortOrder) {

        SQLiteQueryBuilder sqb = new SQLiteQueryBuilder();
        // If this is a row query, limit the result set to the passed in row.
        switch (Constants.URI_MATCHER.match(uri)) {
            case Constants.ALLROWS:
                sqb.setTables(Contract.TABLE_NOTE_NAME);
                break;

            case Constants.SINGLE_ROW:
                sqb.setTables(Contract.TABLE_NOTE_NAME);
                String rowID = uri.getPathSegments().get(1);
                sqb.appendWhere(Contract.KEY_ID + " = " + rowID
                        + (!TextUtils.isEmpty(selection) ?
                        " AND (" + selection + ')' : ""));

                Log.d("S_ROW", "URI: " + uri);
                break;

            case Constants.SOME_ROWS:
                sqb.setTables(Contract.TABLE_NOTE_NAME);
                String rowIds = uri.getPathSegments().get(1);
                sqb.appendWhere(Contract.KEY_ID + " IN (" + rowIds + ")"
                        + (!TextUtils.isEmpty(selection) ?
                        " AND (" + selection + ')' : ""));

                Log.d("MULTI_QUERY", "URI: " + uri);
                break;

            default:
                break;
        }
        // Open a read-only database.
        SQLiteDatabase mDb = todoOpenHelper.getReadableDatabase();

        // Replace these with valid SQL statements if necessary.
        final Cursor cursor = sqb.query(mDb, projection, selection,selectionArg, Contract.GROUP_BY, Contract.HAVING, sortOrder);

        if (cursor != null) {
            cursor.setNotificationUri(getContext().getContentResolver(),uri);
        }

        return cursor;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {

        if(uri.equals(Contract.CONTENT_URI)){
            // Open a read / write database to support the transaction.
            SQLiteDatabase mDb = todoOpenHelper.getWritableDatabase();

            /** To add empty rows to your database by passing in an empty Content Values
             *  object, you must use the null column hack parameter to specify the name of
             *  the column that can be set to null.
             */
            String nullColumnHack = null;

            Log.d("VALUES", values.toString());

            // Insert the values into the table
            long id = mDb.insert(Contract.TABLE_NOTE_NAME, nullColumnHack, values);

            if (id != -1) {
                // Construct and return the URI of the newly inserted row.
                Uri insertedId = ContentUris.withAppendedId(Contract.CONTENT_URI, id);
                // Notify any observers of the change in the data set.
                getContext().getContentResolver().notifyChange(insertedId, null,true);

                Log.d("S_ROW", "URI: " + insertedId);

                return insertedId;
            }

        }
        return null;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArg) {
        // Open a read / write database to support the transaction.
        SQLiteDatabase mDb = todoOpenHelper.getWritableDatabase();

        // If this is a row URI, limit the deletion to the specified row.
        switch (Constants.URI_MATCHER.match(uri)) {
            case Constants.ALLROWS:
                // To return the number of deleted items, you must specify a where
                // clause. To delete all rows and return a value, pass in “1”.
                if (selection == null)  throw new UnsupportedOperationException("Can't delete all todos");

                Log.d("S_ROW", "URI: " + uri);

                break;

            case Constants.SINGLE_ROW:
                String rowID = uri.getPathSegments().get(1);
                selection = Contract.KEY_ID + "=" + rowID
                        + (!TextUtils.isEmpty(selection) ? //is.Empty method returns true if the string is null or 0-length.
                        " AND (" + selection + ')' : "");


                Log.d("S_ROW", "URI: " + uri);

                break;

            case Constants.SOME_ROWS:
                String rowIds = uri.getPathSegments().get(1);
                selection = Contract.KEY_ID + " IN (" + rowIds + ")"
                        + (!TextUtils.isEmpty(selection) ?
                        " AND (" + selection + ')' : "");

                Log.d("MULTIPLE_DELETE", "Delete Multiple: " + uri);

                break;

            default:
                throw new UnsupportedOperationException("Can't delete at specific position");
        }
        // To return the number of deleted items, you must specify a where
        // clause. To delete all rows and return a value, pass in “1”.
        // Execute the deletion.
        int deleteCount = mDb.delete(Contract.TABLE_NOTE_NAME, selection, selectionArg);
        if (deleteCount > 0) {
            // Notify any observers of the change in the data set.
            getContext().getContentResolver().notifyChange(uri, null);
        }

        return deleteCount;
    }

    @Override
    public int update(Uri uri, ContentValues values,String selection, String[] selectionArg) {
        // Open a read / write database to support the transaction.
        SQLiteDatabase mDb = todoOpenHelper.getWritableDatabase();

        // If this is a row URI, limit the deletion to the specified row.
        switch (Constants.URI_MATCHER.match(uri)) {

            case Constants.SINGLE_ROW :
                String rowID = uri.getPathSegments().get(1);
                selection = Contract.KEY_ID + "=" + rowID
                        + (!TextUtils.isEmpty(selection) ? " AND (" + selection + ')' : "");

                Log.d("S_ROW", "URI: " + uri);

                break;

            case Constants.SOME_ROWS :
                String rowIds = uri.getPathSegments().get(1);
                selection = Contract.KEY_ID + " IN (" + rowIds + ")"
                        + (!TextUtils.isEmpty(selection) ?
                        " AND (" + selection + ')' : "");

                Log.d("S_ROW", "URI: " + uri);

                break;

            default:
                break;
        }

        // Perform the update.
        int updateCount = mDb.update(Contract.TABLE_NOTE_NAME,values, selection, selectionArg);
        // Notify any observers of the change in the data set.
        if(updateCount>0)
            getContext().getContentResolver().notifyChange(uri, null);

        return updateCount;
    }
}