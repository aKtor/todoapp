package it.jdk.lab.todo.network;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import com.google.android.gms.auth.GoogleAuthException;
import com.google.android.gms.auth.GoogleAuthUtil;
import com.google.android.gms.auth.GooglePlayServicesAvailabilityException;
import com.google.android.gms.auth.UserRecoverableAuthException;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import it.jdk.lab.todo.R;

/**
 * Created by Flavio on 02/12/13.
 */
public class AuthenticationActivity extends Activity {

    private static final String TAG = "Authentication";
    private static final String KEY_JSON = "JSON_RESP";
    private static final String KEY_TOKEN = "TOKEN";
    private static final String AUDIENCE = "audience:server:client_id:";
    private static final String CLIENT_ID = "846170439833-fhs4tk4g5ce6hj17b25hkstcm1f3rjbv.apps.googleusercontent.com";

    private static final String SCOPE = AUDIENCE + CLIENT_ID;
    private String token = "";
    private String jsonResp = "";
    private TextView tvOutCome;
    private TextView tvResponse;
    private ControllerToken verifyToken;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.autenticate_activity);

        tvOutCome = (TextView) findViewById(R.id.tv_jsonOutCome);
        tvResponse = (TextView) findViewById(R.id.tv_response);

        verifyToken = new ControllerToken();
        new UserAuthorization().execute(getUserEmails());

    }

    private String[] getUserEmails() {
        AccountManager manager = AccountManager.get(getApplicationContext());
        Account[] accounts = manager.getAccountsByType("com.google");
        String[] names = new String[accounts.length];
        for (int i = 0; i < accounts.length; i++) {
            names[i] = accounts[i].name;
            Log.d(TAG, names[i]);
        }
        return names;
    }

    private class UserAuthorization extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            try {
                Log.d(TAG, "param[0] " + params[0]);
                Log.d(TAG, "SCOPE " + SCOPE);

                token = GoogleAuthUtil.getToken(getApplicationContext(), params[0], SCOPE);
                jsonResp = verifyToken.verify(token);
                JSONObject json = new JSONObject(jsonResp);

                Log.d(TAG, "String JSON: " + jsonResp);
                Log.d(TAG, "Json: " + json.toString());

            } catch (GooglePlayServicesAvailabilityException e) {
                Log.d(TAG, "GooglePlayServicesAvailabilityException = " + e.getMessage());
            } catch (UserRecoverableAuthException e) {
                Log.d(TAG, "UserRecoverableAuthException = " + e.getMessage());
            } catch (IOException e) {
                Log.d(TAG, "IOException = " + e.getMessage());
            } catch (GoogleAuthException e) {
                Log.d(TAG, "GoogleAuthException = " + e.getMessage());
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return token;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            refresh();
            Log.d(TAG, "token = " + token);
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(KEY_TOKEN, token);
        outState.putString(KEY_JSON, jsonResp);
    }

    private void refresh() {
        tvOutCome.setText((token == null || "".equals(token)) ? "NULL" : token);
        tvResponse.setText((jsonResp == null || "".equals(jsonResp)) ? "NULL" : jsonResp);
    }
}
