package it.jdk.lab.todo.network;

import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by Flavio on 03/12/13.
 */
public class ControllerToken {

    private static final String URL = "https://www.googleapis.com/oauth2/v1/tokeninfo?id_token=";

        public String verify(String token) {
            try {
                java.net.URL url = new URL(URL + token);
                Log.d("Authentication", "URL + token = " + URL + token);
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.connect();
                int status = conn.getResponseCode();
                InputStream is = null;
                BufferedReader br = null;
                if (status == 200) {
                    is = conn.getInputStream();
                    br = new BufferedReader(new InputStreamReader(is));
                    String line;
                    StringBuilder sb = new StringBuilder();
                    while ((line = br.readLine()) != null) {
                        sb.append(line).append("\n");
                    }
                    return sb.toString();
                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }
}
