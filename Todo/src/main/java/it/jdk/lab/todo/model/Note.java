package it.jdk.lab.todo.model;

/**
 * Created by Jad1 on 16/10/13.
 */
public class Note {
    private int id;
    private String myMemo;
    private String myDate;
    private boolean myCheck;

    public Note () {

    }

    public Note(String memo, String date) {

        this.myMemo = memo;
        this.myDate = date;
        this.myCheck = false;
    }

    public Note(String string) {

    }

    public String getMyMemo() {
        return myMemo;
    }

    public void setMyMemo(String myMemo) {
        this.myMemo = myMemo;
    }

    public String getMyDate() {
        return myDate;
    }

    public void setMyDate(String myDate) {
        this.myDate = myDate;
    }

    public boolean isMyCheck() {  return myCheck;   }

    public void setMyCheck(boolean check) {   this.myCheck = check; }

    public int getId() { return id; }

    public void setId(int id) { this.id = id; }

}


