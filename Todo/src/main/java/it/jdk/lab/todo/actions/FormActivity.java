package it.jdk.lab.todo.actions;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.widget.Toast;

import it.jdk.lab.todo.R;
import it.jdk.lab.todo.list.ListActivity;
import it.jdk.lab.todo.util.OnActionListener;

/**
 * Created by Frankie on 23/10/13.
 */
public class FormActivity extends ActionBarActivity implements OnActionListener {

    public final static int INS_TITLE = 0;
    public final static int MOD_TITLE = 1;
    public final static String TITLE_KEY = "formTitle";

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.form_activity);

    }



    @Override
    public void onAction(int actionId, Bundle bundle) {
        if(actionId == FormFragment.FIELDS_EMPTY){
            CharSequence text = " Devi riempire tutti i campi! ";
            int duration = Toast.LENGTH_SHORT;
            Toast toast = Toast.makeText(this, text, duration);
            toast.show();
        }
        else{
            Intent form_intent = new Intent(FormActivity.this, ListActivity.class);
            form_intent.putExtras(bundle);
            setResult(RESULT_OK, form_intent);

            int title = bundle.getInt(TITLE_KEY);
            if(title==MOD_TITLE){
                CharSequence text = " La nota è stata modificata correttamente! ";
                int duration = Toast.LENGTH_LONG;
                Toast toast = Toast.makeText(this, text, duration);
                toast.show();
            }
            else if(title==INS_TITLE){
                CharSequence text = " La nota è stata aggiunta correttamente! ";
                int duration = Toast.LENGTH_SHORT;
                Toast toast = Toast.makeText(this, text, duration);
                toast.show();
            }
            finish();
        }
    }
}

