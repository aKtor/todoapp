package it.jdk.lab.todo.actions;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

import it.jdk.lab.todo.R;
import it.jdk.lab.todo.list.LineItemActivity;

public class DeleteActivity extends Activity implements OnClickListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.delete_activity);

        final Button BtnYes = (Button) findViewById(R.id.confirmDel_id);
        final Button BtnNo = (Button) findViewById(R.id.negateDel_id);

        BtnYes.setOnClickListener(this);
        BtnNo.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        switch(view.getId()){
            case R.id.confirmDel_id:
                Intent conf_intent = new Intent(this, LineItemActivity.class);
                setResult(RESULT_OK, conf_intent);
                finish();
                break;

            case R.id.negateDel_id:
                Intent del_intent = new Intent(this, LineItemActivity.class);
                setResult(RESULT_CANCELED, del_intent);
                finish();
                break;
        }
    }

}
